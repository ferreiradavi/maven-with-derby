package br.com.maven.main;

import java.util.Calendar;
import java.util.GregorianCalendar;

import br.com.maven.bo.ProductBO;
import br.com.maven.entity.Product;
import br.com.maven.exception.CommitErrorException;

public class Teste {

	public static void main(String[] args) {

		Product product = new Product();
		ProductBO bo = new ProductBO();
		
		product.setId(0);
		product.setDescription("Tipo 1");
		product.setDiscont(5);
		product.setName("Arroz Camil");
		product.setPrice(10.9);
		product.setQuantity(10000);
		product.setRegisterDate(new GregorianCalendar(2017,Calendar.OCTOBER,1));
		
		try {
			bo.register(product);
			System.out.println("Product Registered with Sucess!");
		} catch (CommitErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
