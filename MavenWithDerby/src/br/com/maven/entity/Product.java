package br.com.maven.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement
@Entity
@TableGenerator(name = "SQ_TB_PRODUCT", table = "TB_PRODUCT", pkColumnName = "CD_PRODUCT", 
valueColumnName = "seq_value", pkColumnValue = "Persistence_seq", initialValue = 1, allocationSize = 1)
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SQ_TB_PRODUCT")
	@Column(name="CD_PRODUCT")
	private int id;
	
	@Column(name="DS_NAME")
	private String name;
	
	@Column(name="DS_DESCRIPTION")
	private String description;
	
	@Column(name="VL_PRICE")
	private double price;
	
	@Column(name="VL_DISCONT")
	private double discont;
	
	@Column(name="NR_QUANTITY")
	private int quantity;
	
	@Column(name="DT_REGISTER")
	private Calendar registerDate;
	
	
	//Product also has a Many to One relationship
	//with itself (Many Products to one Parent Product) 
	@ManyToOne(optional=true, fetch=FetchType.LAZY)
	@JoinColumn(name="TB_PRODUCT_PARENT")
	private Product parentProduct;
	
	@OneToMany(mappedBy="parentProduct")
	@JsonIgnore
	private List<Product> parentProducts = new ArrayList<Product>();
	
	public void addProducts(Product newParentProduct) {
		//This association is a bidirectional
		newParentProduct.setParentProduct(this);
		this.parentProducts.add(parentProduct);
	}

	// We have a Product Entity with One to Many relationship with Image entity
	@OneToMany(mappedBy="product",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Image> images;
	
	public void addImages(Image newImage) {
		//This association is a bidirectional
		newImage.setProduct(this);
		this.images.add(newImage);
	}
	
	
	//GETS AND SETS
	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	public List<Product> getParentProducts() {
		return parentProducts;
	}

	public void setParentProducts(List<Product> parentProducts) {
		this.parentProducts = parentProducts;
	}
	
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getDiscont() {
		return discont;
	}

	public void setDiscont(double discont) {
		this.discont = discont;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Calendar getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Calendar registerDate) {
		this.registerDate = registerDate;
	}
	
	//FULL CONSTRUCTOR
	public Product(int id, String name, String description, double price, double discont, int quantity,
			Calendar registerDate) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.discont = discont;
		this.quantity = quantity;
		this.registerDate = registerDate;
	}

	//EMPTY CONSTRUCTOR
	public Product() {
		super();
	}

	
	//TOSTRING
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", price=" + price
				+ ", discont=" + discont + ", quantity=" + quantity + ", registerDate=" + registerDate + "]";
	}
	
	
}
