package br.com.maven.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@TableGenerator(name = "SQ_TB_IMAGE", table = "TB_IMAGE", pkColumnName = "CD_IMAGE", 
valueColumnName = "seq_value", pkColumnValue = "Persistence_seq", initialValue = 1, allocationSize = 1)
public class Image {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SQ_TB_IMAGE")
	@Column(name="CD_IMAGE")
	private int id;
	
	@Lob
	private byte[] image;
	
	@ManyToOne(optional=true, fetch=FetchType.LAZY)
	@JoinColumn(name="CD_PRODUCT")
	private Product product;

	
	//GETS AND SETS
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	
	//FULL CONSTRUCTOR
	public Image(int id, byte[] image) {
		super();
		this.id = id;
		this.image = image;
	}

	//EMPTY CONSTRUCTOR
	public Image() {
		super();
	}
	
	
	

}
