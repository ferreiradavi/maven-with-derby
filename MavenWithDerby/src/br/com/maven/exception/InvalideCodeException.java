package br.com.maven.exception;

public class InvalideCodeException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalideCodeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalideCodeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalideCodeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalideCodeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalideCodeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
