package br.com.maven.singleton;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactorySingleton {

	//Atributo est�tico
	private static EntityManagerFactory factory;
	
	//Construtor privado
	private EntityManagerFactorySingleton(){}
	
	//M�todo getInstance
	public static EntityManagerFactory getInstance(){
		if (factory == null){
			factory = Persistence
				.createEntityManagerFactory("derby");
		}
		return factory;
	}
	
}
