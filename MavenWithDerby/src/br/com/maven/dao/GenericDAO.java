package br.com.maven.dao;

import java.util.List;

import br.com.maven.exception.CommitErrorException;
import br.com.maven.exception.InvalideCodeException;

public interface GenericDAO<T,K> {
	
	void register	(T entity);
	void update		(T entity);
	void delete		(K code) throws InvalideCodeException;
	T search		(K code);
	List<T> list();
	void commit() throws CommitErrorException;

}
