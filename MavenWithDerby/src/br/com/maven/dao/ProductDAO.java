package br.com.maven.dao;

import java.util.List;

import br.com.maven.entity.Product;

public interface ProductDAO extends GenericDAO<Product,Integer>{


	List<Product> listNoRelations();

}
