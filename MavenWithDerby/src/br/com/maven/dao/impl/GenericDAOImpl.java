package br.com.maven.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.maven.dao.GenericDAO;
import br.com.maven.exception.CommitErrorException;
import br.com.maven.exception.InvalideCodeException;

public class GenericDAOImpl<T,K> implements GenericDAO<T,K>{
	
	protected EntityManager em;
	
	private Class<T> clazz;
	
	
	@SuppressWarnings("all")
	public GenericDAOImpl(EntityManager em) {
		this.em = em;
		clazz = (Class<T>) ((ParameterizedType) getClass()
		.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public void register(T entity) {
		em.persist(entity);
	}

	@Override
	public void update(T entity) {
		em.merge(entity);
	}

	@Override
	public void delete(K code) throws InvalideCodeException{
		T entity = search(code);
		if (entity == null) {
			throw new InvalideCodeException("Invalide Code");
		}
		em.remove(entity);
		
	}

	@Override
	public T search(K code) {
		return em.find(clazz, code);
	}

	@Override
	public List<T> list() {
		return em.createQuery("from " + clazz.getName(), clazz).getResultList();
	}

	@Override
	public void commit() throws CommitErrorException {
		try{
			em.getTransaction().begin();
			em.getTransaction().commit();
		}catch(Exception e){
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			e.printStackTrace();
			throw new CommitErrorException("Commit Error!");
		}
	}

}
