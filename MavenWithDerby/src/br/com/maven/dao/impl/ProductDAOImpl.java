package br.com.maven.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.maven.dao.ProductDAO;
import br.com.maven.entity.Product;

public class ProductDAOImpl extends GenericDAOImpl<Product, Integer> implements ProductDAO{

	public ProductDAOImpl(EntityManager em) {
		
		super(em);
		
	}

//	int id, String name, String description, double price, double discont, int quantity,
//	Calendar registerDate
	//This method list all products without relationships with Image and Products
	public List<Product> listNoRelations() {
		return em.createNativeQuery("SELECT p.id \r\n" + 
				"FROM   TB_PRODUCT p \r\n" + 
				"LEFT   JOIN TB_IMAGE i USING (id)  \r\n" + 
				"WHERE  i.id IS NULL",Product.class).getResultList();
	}

}
