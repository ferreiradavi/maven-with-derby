package br.com.maven.dao;

import br.com.maven.entity.Image;

public interface ImageDAO extends GenericDAO<Image,Integer>{

}
