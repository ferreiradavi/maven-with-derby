package br.com.maven.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import br.com.maven.bo.ProductBO;
import br.com.maven.entity.Product;
import br.com.maven.exception.CommitErrorException;

@Path("/product")
public class ProductResource {
	
	private ProductBO bo = new ProductBO();
	
	
//	This method needs a tool for the "POSTMAN" chrome. 
//	You can register products using JSON standard;
//			EX:
//			{
//			"name": "PS4",
//			"price": 560,
//			"quantity": 10000
//			}
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response register(Product product, @Context UriInfo uriInfo){
		try {
			bo.register(product);
			UriBuilder uri = UriBuilder.fromPath(uriInfo.getPath());
			uri.path(String.valueOf(product.getId()));
			return Response.created(uri.build()).build();
		} catch (CommitErrorException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}
	
//	This method needs a tool for the "POSTMAN" chrome. 
//	You can update products using JSON standard;
//			EX:
//			{
//			"id": 1,
//			"name": "PS4",
//			"description": "Black Series, Disk: 500GB",
//			"price": 560,
//			"quantity": 10000
//			}
//			{
//			"name": "PS4 plus",
//			"description": "Black Series, Disk: 1000GB",
//			"price": 850,
//			"quantity": 5000
//			}
	@PUT
	@Path("/{id}") //URL http://localhost:8080/rest/product/{ID HERE FOR UPDATE}
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(Product product, @PathParam("id") int code ){
		try {
			product.setId(code);
			bo.update(product);
			return Response.ok().build();
		} catch (CommitErrorException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}
	
	
	@DELETE
	@Path("/{id}") //URL http://localhost:8080/rest/product/{ID HERE FOR DELETE}
	public void delete(@PathParam("id") int code){
		try {
			bo.delete(code);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	This method return in original URL "product" the list of all products registered
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> list(){
		return bo.list();
	}
	
	
	//This method list all products without relationships with Image and Products
	@Path("/norelationships") //URL http://localhost:8080/rest/product/norelationships
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> listNoRelations(){
		return bo.listNoRelations();
	}
//	
//	//This method return classes without relationships with Product Entity
//	@Path("/noImages") //URL http://localhost:8080/rest/product/norelationships
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<Product> listNoImages(){
//		return bo.listNoImages();
//	}
//	
//	//This method return classes without relationships with Product Entity
//	@Path("/norelationships") //URL http://localhost:8080/rest/product/norelationships
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public List<Product> listNoRelations(){
//		return bo.listNoRelations();
//	}

}
