package br.com.maven.bo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import br.com.maven.dao.ImageDAO;
import br.com.maven.dao.impl.ImageDAOImpl;
import br.com.maven.entity.Image;
import br.com.maven.exception.CommitErrorException;
import br.com.maven.singleton.EntityManagerFactorySingleton;

public class ImageBO {
	
private EntityManagerFactory factory;
	
	public ImageBO() {
		factory = EntityManagerFactorySingleton.getInstance();
	}
	
	public void register(Image image) throws CommitErrorException {
		EntityManager em = factory.createEntityManager();
		ImageDAO dao = new ImageDAOImpl(em);
		try {
			dao.register(image);
			dao.commit();
		} catch (CommitErrorException e) {		
			e.printStackTrace();
			throw new CommitErrorException(e);
		} finally {
			em.close();
		}
	}
	
	
	public void delete(int code) throws Exception {
		EntityManager em = factory.createEntityManager();
		ImageDAO dao = new ImageDAOImpl(em);
		try {
			dao.delete(code);
			dao.commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
	
	
	public void update(Image image) throws CommitErrorException {
		EntityManager em = factory.createEntityManager();
		ImageDAO dao = new ImageDAOImpl(em);
		
		try {
			dao.update(image);
			dao.commit();
		} catch (CommitErrorException e) {
			e.printStackTrace();
			throw new CommitErrorException(e);
		} finally {
			em.close();
		}
	}
	
	public List<Image> list() {
		EntityManager em = factory.createEntityManager();
		ImageDAO dao = new ImageDAOImpl(em);
		List<Image> lista = dao.list();
		em.close();
		return lista;
	}
	
	public Image search(int id) {
		EntityManager em = factory.createEntityManager();
		ImageDAO dao = new ImageDAOImpl(em);
		Image produto = dao.search(id);
		em.close();
		return produto;
	}
	

}
