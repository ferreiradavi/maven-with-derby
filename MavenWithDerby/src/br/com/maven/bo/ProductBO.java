package br.com.maven.bo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import br.com.maven.dao.ProductDAO;
import br.com.maven.dao.impl.ProductDAOImpl;
import br.com.maven.entity.Product;
import br.com.maven.exception.CommitErrorException;
import br.com.maven.singleton.EntityManagerFactorySingleton;

public class ProductBO {
	
	private EntityManagerFactory factory;
	
	public ProductBO() {
		factory = EntityManagerFactorySingleton.getInstance();
	}
	
	public void register(Product product) throws CommitErrorException {
		EntityManager em = factory.createEntityManager();
		ProductDAO dao = new ProductDAOImpl(em);
		try {
			dao.register(product);
			dao.commit();
		} catch (CommitErrorException e) {		
			e.printStackTrace();
			throw new CommitErrorException(e);
		} finally {
			em.close();
		}
	}
	
	
	public void delete(int code) throws Exception {
		EntityManager em = factory.createEntityManager();
		ProductDAO dao = new ProductDAOImpl(em);
		try {
			dao.delete(code);
			dao.commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
	
	
	public void update(Product product) throws CommitErrorException {
		EntityManager em = factory.createEntityManager();
		ProductDAO dao = new ProductDAOImpl(em);
		
		try {
			dao.update(product);
			dao.commit();
		} catch (CommitErrorException e) {
			e.printStackTrace();
			throw new CommitErrorException(e);
		} finally {
			em.close();
		}
	}
	
	public List<Product> list() {
		EntityManager em = factory.createEntityManager();
		ProductDAO dao = new ProductDAOImpl(em);
		List<Product> lista = dao.list();
		em.close();
		return lista;
	}
	
	public Product search(int id) {
		EntityManager em = factory.createEntityManager();
		ProductDAO dao = new ProductDAOImpl(em);
		Product produto = dao.search(id);
		em.close();
		return produto;
	}

	public List<Product> listNoRelations() {
		EntityManager em = factory.createEntityManager();
		ProductDAO dao = new ProductDAOImpl(em);
		List<Product> lista = dao.listNoRelations();
		em.close();
		return lista;
	}
	
}
